<?php

namespace App\Http\Admin;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Form\FormElements;

class Users extends Section implements Initializable
{
    /**
     * @var \App\Role
     */
    protected $model;

    /**
     * Initialize class.
     */
    public function initialize()
    {

    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-group';
    }

    /**
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function getTitle()
    {
        return 'List';
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::table()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                [
                    AdminColumn::text('id', 'ID')->setWidth('30px'),
                    AdminColumn::text('name', 'Name')->setWidth('30px'),
                    AdminColumn::text('email', 'Email')->setWidth('30px'),
                    AdminColumn::custom('Role', function($item) {
                        return $item->role === \App\Models\User::ROLE_PROF 
                            ? 'Booster' : ($item->role === \App\Models\User::ROLE_ADMIN ? 'Admin' : 'User');
                    })->setWidth('30px'),


                ]
            )->setApply(function ($query) {
                $query->orderBy('id', 'DESC');
            })->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $tabs = AdminDisplay::tabbed();

        $tabs->setTabs(function ($id) {
            $tabs = [];

            $tabs[] = AdminDisplay::tab(new FormElements([
                AdminFormElement::text('name', 'Name')->required(),
                AdminFormElement::select('role', 'Role', [
                    \App\Models\User::ROLE_USER => 'User',
                    \App\Models\User::ROLE_PROF => 'Booster',
                    \App\Models\User::ROLE_ADMIN => 'Admin',
                ])->required(),
            ]))->setLabel('Common');

            return $tabs;
        });

        $form = AdminForm::panel()
            ->addHeader([
                $tabs
            ]);

        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    public function getCreateTitle()
    {
        return 'Add';
    }

    public function isDeletable(\Illuminate\Database\Eloquent\Model $model)
    {
        return true;
    }

    public function isCreatable()
    {
        return false;
    }

    public function isEditable(\Illuminate\Database\Eloquent\Model $model)
    {
        return true;
    }
}