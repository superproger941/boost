<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\OrderRequest;

use App\Models\Review;
use App\Models\Game;
use App\Models\Tarif;
use App\Models\Order;

class GamesController extends Controller
{
    const REVIEWS_LIMIT = 6;

    public function show(Request $request) {
        $slug = $request->name;
        $game = Game::where('slug', $slug)->firstOrFail();

        $reviewsResult = Review::select()
            ->where('game_id', $game->id)
            ->orderBy('id', 'DESC')
            ->limit(self::REVIEWS_LIMIT)
            ->get();
        $reviews = [];
        foreach($reviewsResult as $reviewItem) {
            $reviews[] = $reviewItem;
        }

        $reviews = array_chunk($reviews, 3);

        /////////////////////////////////////

        $tarifsResult = Tarif::select()
            ->where('game_id', $game->id)
            ->orderBy('id', 'DESC')
            ->get();
        $tarifs = [];
        foreach($tarifsResult as $tarif) {
            $tarifs[] = $tarif;
        }

        $tarifs = array_chunk($tarifs, 3);

        return view('gamepage', compact('game', 'reviews', 'tarifs'));
    }

    public function product(Request $request) {
        $tarif = Tarif::where('id', $request->id)->firstOrFail();
        $game = $tarif->game;

        $reviewsResult = Review::select()
            ->where('game_id', $game->id)
            ->orderBy('id', 'DESC')
            ->limit(self::REVIEWS_LIMIT)
            ->get();
        $reviews = [];
        foreach($reviewsResult as $reviewItem) {
            $reviews[] = $reviewItem;
        }

        $reviews = array_chunk($reviews, 3);

        return view('product', compact('tarif', 'game', 'reviews'));
    }

    public function orderHandler(OrderRequest $request) {
        $user = \Auth::user();
        $data = $request->all();
        $options = \Arr::get($data, 'OPTIONS');
        unset($data['OPTIONS']);
        unset($data['_token']);

        $data['client_id'] = $user->id;

        $order = new Order;
        $order->fill($data);
        $order->save();

        foreach($options as $optionId => $optionValue) {
            \DB::table('orders_options')->insert([
                'order_id' => $order->id,
                'option_id' => $optionId,
                'created_at' => date('Y-m-d H:i:s'),
            ]);
        }
        
        return redirect(route('home'))->withSuccess('Order has been maked.');
    }
}
