<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;
use App\Models\Blog;
use App\Models\Review;

class MainController extends Controller
{
    const REVIEWS_LIMIT = 6;
    const REVIEWS_PER_PAGE = 9;

    public function show() {
        $reviewsResult = Review::select()
            ->orderBy('id', 'DESC')
            ->limit(self::REVIEWS_LIMIT)
            ->get();
        $reviews = [];
        foreach($reviewsResult as $reviewItem) {
            $reviews[] = $reviewItem;
        }

        $reviews = array_chunk($reviews, 3);

        return view('index', compact('reviews'));
    }

    public function page(Request $request) {
        $slug = $request->slug;
        $page = Page::where('slug', $slug)->firstOrFail();

        return view('page', [
            'content' => $page->content,
        ]);
    }

    public function blogs(Request $request) {
        $blog = Blog::select()->with('game')->paginate(9);
        $blogArray = [];

        foreach($blog as $item) {
            $blogArray[] = $item;
        }

        $chunks = array_chunk($blogArray, 3);

        return view('blog', [
            'chunks' => $chunks,
        ]);
    }

    public function reviews(Request $request) {
        $reviews = Review::select()->with('game', 'user')->paginate(self::REVIEWS_PER_PAGE);

        return view('reviews', [
            'reviews' => $reviews,
        ]);
    }

    public function blog(Request $request) {
        $slug = $request->slug;
        $page = Blog::where('slug', $slug)->firstOrFail();

        return view('page', [
            'content' => $page->content,
        ]);
    }
}
