<?php

namespace App\Providers;

use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
        \App\Models\User::class => 'App\Http\Admin\Users',
        \App\Models\Page::class => 'App\Http\Admin\Pages',
        \App\Models\Meta::class => 'App\Http\Admin\Metas',
        \App\Models\Game::class => 'App\Http\Admin\Games',
        \App\Models\Contact::class => 'App\Http\Admin\Contacts',
        \App\Models\Setting::class => 'App\Http\Admin\Settings',
        \App\Models\Block::class => 'App\Http\Admin\Blocks',
        \App\Models\Blog::class => 'App\Http\Admin\Blogs',
        \App\Models\Social::class => 'App\Http\Admin\Socials',
        \App\Models\Review::class => 'App\Http\Admin\Reviews',
        \App\Models\Tarif::class => 'App\Http\Admin\Tarifs',
        \App\Models\Order::class => 'App\Http\Admin\Orders',
        \App\Models\OptionGroup::class => 'App\Http\Admin\OptionGroups',
        \App\Models\Option::class => 'App\Http\Admin\Options',
    ];

    /**
     * Register sections.
     *
     * @param \SleepingOwl\Admin\Admin $admin
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
    	//

        parent::boot($admin);
    }
}
