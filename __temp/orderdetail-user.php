<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Boosting Service</title>

	<!-- responsive meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- For IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    

	<link rel="stylesheet" href="assets/css/font-awesome.min.css">
 

    <link rel="stylesheet" href="assets/css/style.css">
	<link rel="stylesheet" href="assets/css/responsive.css">

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="assets/images/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="assets/images/favicon/favicon-16x16.png" sizes="16x16">

    <!-- Fixing Internet Explorer-->
    <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <script src="assets/js/html5shiv.js"></script>
    <![endif]-->
    
</head>

<body>


<div class="boxed_wrapper">

    <profile class="profile">

        <div class="profile-aling">

            <div class="profile-block-1">

                <img src="./assets/images/details/logo.gif" class="footer-logo">

            </div>
    
            <div class="profile-menu">

                <div class="profile-avatar-aling">
                    <div class="profile-avatar-2">
                        <img class="profile-ava-3" src="./assets/images/avatars/1.png">
                    </div>
                </div>

                <div class="profile-menu-block">
                    <h2>Walter</h2>
                    <a href="#">wallhack@gmail.com</a>
                </div>

            </div>

            <div class="profile-menu">

                <div class="profile-avatar-aling">
                    <div class="profile-avatar">
                        <img class="profile-ava" src="./assets/images/balance.png">
                    </div>
                </div>

                <div class="balance-menu-block">

                    <h2>Balance:</h2>

                    <div class="blance-available-aling">

                        <div class="balance-available-4">
                            <p>Bonuce: <span style="color: white;">424 coins</span></p>
                        </div>

                        <div class="balance-available-2">
                            <p>Spend: <span style="color: white;">1,224$</span></p>
                        </div>

                    </div>

                </div>

                <div class="out-edit">

                    <div class="out">
                    <a href="index.html">Log out</a>
                    <img src="./assets/images/log.png" class="out-icon">
                    </div>
        
                    <div class="edit-prof">
                        <a href="#">Edit Profile</a>
                        <img src="./assets/images/edit.png" class="edit-prof-icon">
                    </div>
        
                </div>

            </div> 

        </div>


    </profile>

    <orderlist class="order-list">

        <div class="order-list-menu">

            <div class="order-list-menu-aling">

                <div class="order-pad-4">
                    <img src="./assets/images/progress.gif" class="order-icon">
                    <a href="#">In progress</a>
                </div>

                <div class="order-pad-4">
                    <img src="./assets/images/dragonico.png" class="order-icon">
                    <a href="#">End orders</a>
                </div>

                <div class="order-pad-3">
                    <img src="./assets/images/logo-an.gif" class="order-icon">
                    <a href="#">Buy more services</a>
                </div>

                <div class="order-pad-3">
                    <img src="./assets/images/dragonico.png" class="order-icon">
                    <a href="#">Back to home</a>
                </div>
     

            </div>

            <div class="order-title">
                <h1>order details</h1>
            </div>

        </div>

        <div class="order-list-margin">

            <div class="order-list-blocks-detail">

                <div class="order-list-block">
                    <p>Id:<span style="margin-left: 3px; color: white;">1</span></p>
                </div>
                <div class="order-list-block">
                    <p>Game:<span style="margin-left: 3px; color: white;">Apex Legend</span></p>
                </div>
                <div class="order-list-block">
                    <p>Service:<span style="margin-left: 3px; color: white;">Boost</span></p>
                </div>
                <div class="order-list-block">
                    <p>Platform:<span style="margin-left: 3px; color: white;">XBOX</span></p>
                </div>
                <div class="order-list-block">
                    <p>Additional<span style="margin-left: 3px; color: white;">></span></p>
                </div>
                <div class="order-list-block">
                    <p>Price:<span style="margin-left: 3px; color: white;">$25</span></p>
                </div>
                <div class="order-list-block">
                    <p>Status:<span style="margin-left: 3px; color: white;">Pending</span></p>
                </div>
                <div class="order-list-block">
                    <p>Progress:<span style="margin-left: 3px; color: white;">3800 rate</span></p>
                </div>
    
                <button type="submit" id="broke-btn">Broke Task</button>
    
            </div>

            <div class="show-details">

                <div class="show-details-aling">

                    <div class="chat">

                    <div id="chat-bg">

                    <ul id="messages"></ul>
                    <div class="messages-content"></div>

                    </div>

                    <form id="form" onsubmit="return sendMessage();">
                        <div class="input-chat">
                            <input id="message" placeholder="Write Message" autocomplete="off">
                            <button type="submit" id="send-chat">Send</button>
                        </div>
                    </form>

                    <div class="dit-progress-bar">

                        <div class="dit-pg-bar">
                            <img src="./assets/images/progress-dit.png" class="dit-progress-ico">
                            <p>Starting <br> <span>3800</span></p>
                        </div>

                        <div class="dit-pg-bar">
                            <img src="./assets/images/progress-dit.gif" class="dit-progress-ico">
                            <p class="active-dit-pg-bar">Currently <br> <span class="active-dit-pg-span">3800 rate</span></p>
                        </div>

                        <div class="dit-pg-bar">
                            <img src="./assets/images/progress-dit.png" class="dit-progress-ico">
                            <p>Purpose <br> <span>4800</span></p>
                        </div>


                    </div>

                </div>

                    <div class="dit-avatar">
                        <div class="dit-avatar-prof">
                            <img class="profile-ava-2" src="./assets/images/avatars/2.gif">
                        </div>
                        <div class="dit-avatar-name">
                            <h3>Doom</h3>
                            <p>Worked</p>
                        </div>
                        <div class="what-game">
                            <p>Apex Legend</p>
                        </div>
                    </div>

                </div>

            </div>

        </div>


    </orderlist>


    <footer class="footer">

        <div class="footer-aling">

            <div class="footer-block-1">
                <img src="./assets/images/details/logo.gif" class="footer-logo">
                
                <div class="footer-pay">
    
                    <p>Send us a message</p>
    
                    <div class="pay-ico-block">
                        <img src="./assets/images/icons/pay-ico1.png" class="pay-ico">
                    </div>
    
                    <div class="pay-ico-block">
                        <img src="./assets/images/icons/pay-ico1.png" class="pay-ico">
                    </div>
    
                    <div class="pay-ico-block">
                        <img src="./assets/images/icons/pay-ico1.png" class="pay-ico">
                    </div>
    
                    <div class="pay-ico-block" style="border-right: 0px solid #4d395c!important;">
                        <img src="./assets/images/icons/pay-ico1.png" class="pay-ico">
                    </div>
    
                </div>
        
                <form>
    
                    <div class="footer-input">
    
                        <div class="input-data">
                            <input id="message" type="text" placeholder="Your Name" autocomplete="off">
                            <div class="input-ico">
                                <img src="./assets/images/icons/input-1.png" class="inputpng">
                            </div>
                        </div>
    
                        <div class="input-data" style="margin-left: 20px;">
                            <input id="message" type="email" placeholder="Your Mail" autocomplete="off">
                            <div class="input-ico">
                                <img src="./assets/images/icons/input-2.png" class="inputpng">
                            </div>
                        </div>
    
                    </div>
    
    
                    <div class="input-data-2" style="margin-left: 17px;">
                        <textarea id="message" type="text" placeholder="create message..." autocomplete="off"></textarea>
                    </div>
    
                    <button type="submit" id="send-btn">send message</button>
        
                </form>
    
            </div>
    
            <div class="footer-menu">
                <div class="footer-menu-block">
                    <h2>Game</h2>
                    <a href="#">Apex Legend</a>
                    <a href="#">Valorant</a>
                    <a href="#">Dota 2</a>
                    <a href="#">Call of duty: Warzone</a>
                    <a href="#">Call of duty: B.O Cold War</a>
                </div>
            </div>
    
            <div class="footer-menu">
                <div class="footer-menu-block-2">
                    <h2>NAVIGATION</h2>
                    <a href="#">Contacts</a>
                    <a href="#">Loyalty Program</a>
                    <a href="#">How it work ?</a>
                    <a href="#">Blog</a>
                </div>
            </div>
    
            <div class="footer-menu">
                <div class="footer-menu-block-3">
    
                    <div class="comm-block" style="margin-top: -20px;">
                        <img src="./assets/images/coom-ico.png" class="comm-ico">
                        <div class="comm-text-block">
                            <div class="comm-name">
                                <h3>Which games do ?</h3>
                            </div>
                            <p style="width: 220px;">boosting.gg boosting service focused on the most popular online games like Dota 2 League.</p>
                        </div>
                    </div>
    
                    <a href="#">All rights reserved. We care about your privacy.
                        Copyright © 2021 boosting.gg, support@boosting.com</a>
                </div>
            </div>

        </div>


    </footer>

</div> 

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js"></script>

<script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-database.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-analytics.js"></script>

<script>
  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyBgMTXJbeadYDEi32sv0eYlRvt2OHFacG0",
    authDomain: "live-chat-8ca47.firebaseapp.com",
    projectId: "live-chat-8ca47",
    storageBucket: "live-chat-8ca47.appspot.com",
    messagingSenderId: "804223580100",
    appId: "1:804223580100:web:a4d14bee6e54dbac255691",
    measurementId: "G-NRM0F0YGPB"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();


  function sendMessage() {

    var message = document.getElementById("message").value;

    $('#form')[0].reset();

    firebase.database().ref("messages").push().set({
        "sender": 1, 
        "message": message,
    });
    $("#chat-bg").scrollTop($("#chat-bg")[0].scrollHeight);
    return false;

  }

  firebase.database().ref("messages").on("child_added", function (snapshot) {
    if (snapshot.val().sender == 1) {
        var html = "";
        html += '<div class="chat-msg-toyou"><p>';
        html += /*snapshot.val().sender +*/ snapshot.val().message;
        html += "</p></div>";
    } else {
        var html = "";
        html += '<div class="chat-msg-you"><p>';
        html += /*snapshot.val().sender +*/ snapshot.val().message;
        html += "</p></div>";
    }
    document.getElementById("messages").innerHTML += html;
  });




</script>

</body>
</html>

