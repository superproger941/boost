

@extends('layouts.index')
@section('content')

<link rel="icon" type="image/png" href="assets/images/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="assets/images/favicon/favicon-16x16.png" sizes="16x16">

<div class="boxed_wrapper">
    @include('components.header')
    @include('components.pages')
    <gameslide class="gameslide">
        @if(count($reviews))
        <div class="review-details-al">
            <div class="review-details">
                <h2 style="text-align: left!important;">Review from users</h2>
                <div class="review-details-aling">
                @foreach($reviews as $review)
                    <div class="comm-block-2">
                        <img src="{{isset($review->avatar) ? '/'.$review->avatar : '/assets/images/coom-ico.png'}}" class="comm-ico">
                        <div class="comm-text-block">
                            <div class="comm-name">
                                <h3>{{isset($review->user->name) ? $review->user->name : $review->name}}</h3>
                                <p>Verified</p>
                            </div>
                            <p>{{$review->text}}</p>
                        </div>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
        @endif
        {{$reviews->render()}}
    </gameslide>
    @include('components.footer')
</div>
