<div class="order-list-blocks-detail">
        <div class="order-list-block">
        <p>Id:<span style="margin-left: 3px; color: white;">{{$key + 1}}</span></p>
    </div>
    <div class="order-list-block">
        <p>Game:<span style="margin-left: 3px; color: white;">{{$order->tarif->game->name}}</span></p>
    </div>
    <div class="order-list-block">
        <p>Service:<span style="margin-left: 3px; color: white;">{{$order->tarif->name}}</span></p>
    </div>
    <!-- <div class="order-list-block">
        <p>Platform:<span style="margin-left: 3px; color: white;">XBOX</span></p>
    </div> -->
    <div class="order-list-block">
        <p>Additional<span style="margin-left: 3px; color: white;">></span></p>
    </div>
    <div class="order-list-block">
        <p>Price:<span style="margin-left: 3px; color: white;">${{$order->amount}}</span></p>
    </div>
    <div class="order-list-block">
        <p>Status:<span style="margin-left: 3px; color: white;">@include('profile.particles.status')</span></p>
    </div>
    <div class="order-list-block">
        <p>Progress:<span style="margin-left: 3px; color: white;">0%</span></p>
    </div>
    <form action="{{route('order.end')}}" method="POST">
        @csrf
        <input type="hidden" value="{{$order->id}}" name="order_id">
        <input type="hidden" value="3" name="status">
        <button type="submit" id="take-btn">Broke Task</button>
    </form>
</div>
<div class="show-details">
    <div class="show-details-aling">
        <div class="chat">
            <div id="chat-bg">
                <ul id="messages"></ul>
                <div class="messages-content"></div>
            </div>
            <form id="form" onsubmit="return sendMessage();">
                <div class="input-chat">
                    <input id="message" placeholder="Write Message" autocomplete="off">
                    <button type="submit" id="send-chat">Send</button>
                </div>
            </form>
            <div class="dit-progress-bar">
                <div class="dit-pg-bar">
                    <img src="/assets/images/progress-dit.png" class="dit-progress-ico">
                    <p>Starting <br> <span>3800</span></p>
                </div>
                <div class="dit-pg-bar">
                    <img src="/assets/images/progress-dit.gif" class="dit-progress-ico">
                    <p class="active-dit-pg-bar">Currently <br> <span class="active-dit-pg-span">3800 rate</span></p>
                </div>
                <div class="dit-pg-bar">
                    <img src="/assets/images/progress-dit.png" class="dit-progress-ico">
                    <p>Purpose <br> <span>4800</span></p>
                </div>
                @if(\Auth::user()->id === $order->client_id)
                <div class="order-btn-aling">
                    <form action="{{route('order.end')}}" method="POST">
                        @csrf
                        <input type="hidden" value="{{$order->id}}" name="order_id">
                        <input type="hidden" value="2" name="status">
                        <button type="submit" id="end-work">End Work</button>
                    </form>
                    <div class="input-details">
                        <input id="message" type="text" placeholder="Set Progress" autocomplete="off">
                        <button type="submit" id="save-progress">Save</button>
                    </div>

                </div>
                @endif
            </div>
        </div>
        <div class="dit-avatar">
            <div class="dit-avatar-prof">
                <img class="profile-ava-2" src="/assets/images/avatars/2.gif">
            </div>
            <div class="dit-avatar-name">
                <h3>Doom</h3>
                <p>Worked</p>
            </div>
            <div class="what-game">
                <p>Apex Legend</p>
            </div>
        </div>
    </div>
</div>