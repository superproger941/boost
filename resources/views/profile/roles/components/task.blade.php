@if($order->status >= \App\Models\Order::STATUS_INPROGRESS)
<a href="{{route('order.show', ['id' => $order->id])}}">
@endif
    <div class="order-list-blocks">
        <div class="order-list-block">
            <p>Id:<span style="margin-left: 3px; color: white;">{{$key + 1}}</span></p>
        </div>
        <div class="order-list-block">
            <p>Game:<span style="margin-left: 3px; color: white;">{{$order->tarif->game->name}}</span></p>
        </div>
        <div class="order-list-block">
            <p>Service:<span style="margin-left: 3px; color: white;">{{$order->tarif->name}}</span></p>
        </div>
        <!-- <div class="order-list-block">
            <p>Platform:<span style="margin-left: 3px; color: white;">XBOX</span></p>
        </div> -->
        <div class="order-list-block">
            <p>Additional<span style="margin-left: 3px; color: white;">></span></p>
        </div>
        <div class="order-list-block">
            <p>Price:<span style="margin-left: 3px; color: white;">${{$order->amount}}</span></p>
        </div>
        <div class="order-list-block">
            <p>Status:<span style="margin-left: 3px; color: white;">@include('profile.particles.status')</span></p>
        </div>
        <div class="order-list-block">
            <p>Progress:<span style="margin-left: 3px; color: white;">0%</span></p>
        </div>
        <form action="{{route('order.take')}}" method="POST">
            @csrf
            <input type="hidden" value="{{$order->id}}" name="order_id">
            <button type="submit" id="take-btn">Take Order</button>
        </form>
    </div>
@if($order->status >= \App\Models\Order::STATUS_INPROGRESS)
</a>
@endif
