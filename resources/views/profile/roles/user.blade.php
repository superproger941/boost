
<div class="boxed_wrapper">
    @include('profile.particles.header')
    <orderlist class="order-list">
        <div class="order-list-menu">
            @include('profile.particles.topmenu')
            <div class="order-title">
                <!-- <h1>all orders</h1> -->
            </div>
        </div>
        @foreach($orders as $key => $order)
        <!-- <div class="order-list-margin"> -->
            @include('profile.roles.components.task')
        <!-- </div> -->
        @endforeach
    </orderlist>
    <loyalty  class="loyalprogram">
        <div class="loyalprogram-title">
            <img src="/assets/images/loyal.png" class="loyalprogram-title-ico">
            <h1>Loyalty Program</h1>
        </div>
        <div class="loyalprogram-blocks-aling">
            <div class="loyalprogram-blocks">
                <div class="loyalprogram-block">
                    <p>Level 1</p>
                    <img src="/assets/images/loyalrang.gif" class="loyalprogram-block-pic">
                    <h2>Life-Time 3% <br> Cashback In BC</h2>
                </div>
                <div class="loyalprogram-block-text">
                    <p>Spent 50 EUR <br> or 5 orders</p>
                    <h2 class="active-h2">You Here</h2>
                </div>
            </div>
            <div class="loyalprogram-blocks">
                <div class="loyalprogram-block">
                    <p>Level 2</p>
                    <img src="/assets/images/loyalrang.png" class="loyalprogram-block-pic">
                    <h2>Life-Time 3% <br> Cashback In BC</h2>
                </div>
                <div class="loyalprogram-block-text">
                    <p>Spent 50 EUR <br> or 5 orders</p>
                    <h2>100 Eur to bonuce</h2>
                </div>
            </div>
            <div class="loyalprogram-blocks">
                <div class="loyalprogram-block">
                    <p>Level 3</p>
                    <img src="/assets/images/loyalrang.png" class="loyalprogram-block-pic">
                    <h2>Life-Time 3% <br> Cashback In BC</h2>
                </div>
                <div class="loyalprogram-block-text">
                    <p>Spent 50 EUR <br> or 5 orders</p>
                    <h2>100 Eur to bonuce</h2>
                </div>
            </div>
            <div class="loyalprogram-blocks">
                <div class="loyalprogram-block">
                    <p>Level 4</p>
                    <img src="/assets/images/loyalrang.png" class="loyalprogram-block-pic">
                    <h2>Life-Time 3% <br> Cashback In BC</h2>
                </div>
                <div class="loyalprogram-block-text">
                    <p>Spent 50 EUR <br> or 5 orders</p>
                    <h2>100 Eur to bonuce</h2>
                </div>
            </div>
            <div class="loyalprogram-blocks">
                <div class="loyalprogram-block">
                    <p>Level 5</p>
                    <img src="/assets/images/loyalrang.png" class="loyalprogram-block-pic">
                    <h2>Life-Time 3% <br> Cashback In BC</h2>
                </div>
                <div class="loyalprogram-block-text">
                    <p>Spent 50 EUR <br> or 5 orders</p>
                    <h2>100 Eur to bonuce</h2>
                </div>
            </div>
        </div>
    </loyalty>
    @include('components.footer')
</div> 

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js"></script>

<script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-database.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
 https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/8.3.2/firebase-analytics.js"></script>

<script>
// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var firebaseConfig = {
apiKey: "AIzaSyBgMTXJbeadYDEi32sv0eYlRvt2OHFacG0",
authDomain: "live-chat-8ca47.firebaseapp.com",
projectId: "live-chat-8ca47",
storageBucket: "live-chat-8ca47.appspot.com",
messagingSenderId: "804223580100",
appId: "1:804223580100:web:a4d14bee6e54dbac255691",
measurementId: "G-NRM0F0YGPB"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();


function sendMessage() {

var message = document.getElementById("message").value;

$('#form')[0].reset();

firebase.database().ref("messages").push().set({
    "sender": 1, 
    "message": message,
});
$("#chat-bg").scrollTop($("#chat-bg")[0].scrollHeight);
return false;

}

firebase.database().ref("messages").on("child_added", function (snapshot) {
if (snapshot.val().sender == 1) {
    var html = "";
    html += '<div class="chat-msg-toyou"><p>';
    html += /*snapshot.val().sender +*/ snapshot.val().message;
    html += "</p></div>";
} else {
    var html = "";
    html += '<div class="chat-msg-you"><p>';
    html += /*snapshot.val().sender +*/ snapshot.val().message;
    html += "</p></div>";
}
document.getElementById("messages").innerHTML += html;
});

</script>
