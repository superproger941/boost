@extends('layouts.profile')

@section('content')
    <div class="boxed_wrapper">
        @include('profile.particles.header')
        <orderlist class="order-list">
            <div class="order-list-menu">
                @include('profile.particles.topmenu')
                <div class="order-title">
                    <!-- <h1>all orders</h1> -->
                </div>
            </div>
        </orderlist>

        @include('components.footer')
    </div> 
@endsection
