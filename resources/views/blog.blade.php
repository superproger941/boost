@extends('layouts.index')
@section('content')

<link rel="icon" type="image/png" href="assets/images/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="assets/images/favicon/favicon-16x16.png" sizes="16x16">

<div class="boxed_wrapper">
    @include('components.header')
    @include('components.pages')
    <gameslide class="gameslide">
        <div class="gameslide-aling">
            <div class="gameslide-block">
                <img src="/assets/images/game-bg/apexlarge.png" class="game-large">
            </div>
        </div>
        <div class="features-title-2">
            <div class="features-title-aling">
                <div class="features-title-slog" style="margin-left: 587px!important;">
                    <img src="/assets/images/bullet.png" class="features-ico">
                    <p>Hot Blog</p>
                </div>
            </div>
        </div>
        @foreach($chunks as $blogs)
        <div class="game-service-block-aling">
            <img src="" class="">
            @foreach($blogs as $blog)
            <div class="game-service-block">
                <img src="/assets/images/loyal.png" class="game-service-img">
                <div class="game-service-txt-3">
                    <img src="">
                    <p>Blog</p>
                    <h1>{{$blog->meta_h1}}</h1>
                </div>
                <div class="game-service-txt-2">
                    <p>{{$blog->content}}</p>
                </div>
                <div class="">
                    <div class="create-order-pads-3">
                        <a href="{{route('blog.item', ['slug' => $blog->slug])}}" id="buy-ord-btn-3">Open News</a>
                        @if($blog->game)
                            <div class="how-much-3"><p>{{$blog->game->name}}</p></div>
                        @endif
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @endforeach
    </gameslide>
    @include('components.footer')
</div>