
<footer class="footer">
    <div class="footer-aling">
        <div class="footer-block-1">
            <img src="/assets/images/details/logo.gif" class="footer-logo">
            <div class="footer-pay">
                <p>Send us a message</p>
                @foreach(config('socials') as $social)
                <div class="pay-ico-block">
                    <a href="{{$social->link}}">
                        <img src="{{url($social->icon)}}" class="pay-ico">
                    </a>
                </div>
                @endforeach
            </div>
            <form method="POST" action="{{route('contact.send')}}">
                @csrf
                <div class="footer-input">
                    <div class="input-data">
                        <input id="message" name="name" type="text" placeholder="Your Name" autocomplete="off">
                        <div class="input-ico">
                            <img src="/assets/images/icons/input-1.png" class="inputpng">
                        </div>
                    </div>
                    <div class="input-data" style="margin-left: 20px;">
                        <input id="message" name="email" type="email" placeholder="Your Mail" autocomplete="off">
                        <div class="input-ico">
                            <img src="/assets/images/icons/input-2.png" class="inputpng">
                        </div>
                    </div>
                </div>
                <div class="input-data-2" style="margin-left: 17px;">
                    <textarea id="message" name="text" type="text" placeholder="create message..." autocomplete="off"></textarea>
                </div>
                <button type="submit" id="send-btn">send message</button>
            </form>
        </div>
        <div class="footer-menu">
            <div class="footer-menu-block">
                <h2>Game</h2>
                @foreach(config('games') as $game)
                    <a href="{{route('game', ['name' => $game->slug])}}">{{$game->name}}</a>
                @endforeach
            </div>
        </div>
        <div class="footer-menu">
            <div class="footer-menu-block-2">
                <h2>NAVIGATION</h2>
                <a href="{{route('contacts')}}">Contacts</a>
                <a href="{{route('page', ['slug' => 'loyalityprogram'])}}">Loyalty Program</a>
                <a href="{{route('page', ['slug' => 'howitwork'])}}">How it work ?</a>
                <a href="{{route('blog')}}">Blog</a>
            </div>
        </div>
        <div class="footer-menu">
            <div class="footer-menu-block-3">
                <div class="comm-block" style="margin-top: -20px;">
                    <img src="/assets/images/coom-ico.png" class="comm-ico">
                    <div class="comm-text-block">
                        <div class="comm-name">
                            <h3>{{\Arr::get(config('blocks'), 'block_19.title')}}</h3>
                        </div>
                        <p style="width: 220px;">{{\Arr::get(config('blocks'), 'block_19.content')}}</p>
                    </div>
                </div>
                <a href="{{route('main')}}">{{config('copyright')}}</a>
            </div>
        </div>
    </div>
</footer>
